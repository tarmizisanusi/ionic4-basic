# ionic4-basic

## Step Android Build Process

### Step 1 - Run a Production Build

        ionic cordova build android --prod --release

### Step 2 - Generate a Keystore

        keytool -genkey -v -keystore my-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-alias

### Step 3 - Sign the APK

        jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk my-alias


### Step 4 - Figure out your build tools path

        printenv ANDROID_HOME

    Output shoud be:  ~/Library/Android/sdk/. 
    
        ls ~/Library/Android/sdk/build-tools

    Output should be: 28.0.3 version number
    
    So replace {build-tools-path} with the path to build tools on your machine for the following commands, i.e. ~/Library/Android/sdk/build-tools/28.0.3
    
### Step 5 - Run zipalign

        {build-tools-path}/zipalign -v 4 android-release-unsigned.apk YourAppName-Release.apk

### Step 6 - Verify the Signature

        {build-tools-path}/apksigner verify YourAppName-Release.apk


